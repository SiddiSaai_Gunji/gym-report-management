package com.epam.gymreportmanagement.exception;

public class ReportException extends Exception{
    public ReportException(String message) {
        super(message);
    }
}
